docker-compose profiles for various backpack.tf configurations.

Keeps server configurations consistent and clean.

# Configurations

## frontend

`frontend` is a configuration that includes nginx, php-fpm and mongodb for frontend servers.

## item-stats

`item-stats` contains the elasticsearch stats cluster.

## inventory-store

`inventory-store` contains configuration for the GridFS inventory store.

## inventory-store-arbiter

`inventory-store-arbiter` contains configuration for an arbiter for the GridFS inventory store (as we only have two servers for this).

# Overriding

These configs are based around how the backpack.tf servers are configured, however the volume mappings may not match your development environment.

docker-compose will by default extend a configuration with `docker-compose.override.yml` if it exists. This makes overriding for development easy.

For example, the `frontend` configuration could be extended with an override file looking like this:
 
```
version: '3'
services:
  nginx:
    volumes:
      - /Users/fiskie/bptf/backpack.tf:/var/www/bptf
  php-fpm:
    volumes:
      - /Users/fiskie/bptf/backpack.tf:/var/www/bptf
  mongo:
    volumes:
      - /usr/local/var/mongodb:/data/db
```

This will replace the host:container volume mappings correspondingly.